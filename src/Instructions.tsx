import { FormattedMessage } from 'react-intl'

export default function render() {
    return (
        <div>
            <h1 className="no-print"><FormattedMessage id="title" defaultMessage="Vaccination Proof Card Generator" /></h1>
            <section className="no-print instructions">
                <p className="alert">
                    <FormattedMessage id="disclaimer" defaultMessage="All processing done by this application happens within your browser. No data is sent across the network or to any third party." />
                </p>
                <ol>
                    <li><FormattedMessage id="instructions.1" defaultMessage="Scan the QR code of your vaccination proofs using your camera" /></li>
                    <li>
                        <FormattedMessage
                            id="instructions.2"
                          defaultMessage="Print this page, using <kbd>CTRL+P</kbd> or from the browser's menu, or <button>click here</button>"
                            values={{
                                kbd: (chunk: JSX.Element) => <kbd>{chunk}</kbd>,
                                button: (chunk: JSX.Element) => <button onClick={window.print}>{chunk}</button>
                            }}
                        />
                    </li>
                    <li><FormattedMessage id="instructions.3" defaultMessage="Cut out then glue the card wherever you like" /></li>
                </ol>
            </section>
        </div>
    );
}
