import { QRCode } from 'jsqr';
import React from 'react';
import pako from 'pako';
import jws from 'jws'
import update from 'immutability-helper'
import Scanner from './Scanner';
import { FHIRBundle, getResources } from './lib/extract';
import Card from './Card';

import './App.css'

const QR_NUMERIC_OFFSET = 45;

const parseBase64Json = (b64: string) => JSON.parse(
  Buffer.from(b64, 'base64').toString()
);

// https://spec.smarthealth.cards/#encoding-chunks-as-qr-codes
function parseCode(qr: QRCode) {
  let code = qr.data;
  console.log("process: ", code);

  if (!code.startsWith('shc:/')) {
    throw 'Code does not start with shc:/';
  }

  code = code.slice(5);

  if (code.length % 2 != 0) {
    throw 'Code has a odd number of digits';
  }

  // TODO Check for 1/2 style multi-chunks.
  let jws = '';

  for (let i = 1; i < code.length; i += 2) {
    const digit = parseInt(code[i - 1] + code[i]);

    jws += String.fromCharCode(digit + QR_NUMERIC_OFFSET);
  }

  return jws
}

export function parseJws(jws: string) {
  const parts = jws.split(".");

  if (parts.length != 3) {
    throw 'Invalid JWS token expected 3 parts it has ' + parts.length;
  }

  // JWS Header
  // header includes alg: "ES256"
  // header includes zip: "DEF"
  // header includes kid equal to the base64url-encoded SHA-256 JWK Thumbprint of the key (see RFC7638)
  const header = parseBase64Json(parts[0]);

  // JWS Payload
  // payload is minified (i.e., all optional whitespace is stripped)
  // payload is compressed with the DEFLATE (see RFC1951) algorithm before being signed (note, this should be "raw" DEFLATE compression, omitting any zlib or gz headers)
  // payload .vc.credentialSubject.fhirBundle is created:
  // 		without Resource.id elements
  // 		without Resource.meta elements (or if present, .meta.security is included and no other fields are included)
  // 		without Resource.text elements
  // 		without CodeableConcept.text elements
  // 		without Coding.display elements
  // 		with Bundle.entry.fullUrl populated with short resource-scheme URIs (e.g., {"fullUrl": "resource:0})
  // 		with Reference.reference populated with short resource-scheme URIs (e.g., {"patient": {"reference": "resource:0"}})

  const decode = (payload: string) => {
    let decoded: any = Buffer.from(payload, 'base64')
    decoded = pako.inflateRaw(decoded, { to: 'string' });
    decoded = JSON.parse(decoded);

    return decoded as object;
  }

  let payload: any;

  if ('zip' in header) {
    if (header['zip'] == 'DEF') {
      payload = decode(parts[1]);
    } else {
      throw 'Unsupported compression ' + header['zip'];
    }
  } else {
    payload = parseBase64Json(parts[1])
  }

  let signature = Buffer.from(parts[2], 'base64')

  return {
    'header': header,
    'payload': payload,
    'signature': signature,
  }
}

export interface PropsType {
  pubkey: string;
}

interface StateType {
  entries: ScanResult[];
  error: Error | null;
}

interface ScanResult {
  shcUri: string;
  fhirBundle: FHIRBundle;
  verified: boolean;
}

export default class App extends React.Component<PropsType, StateType> {
  state = {
    entries: new Array<ScanResult>(),
    error: null,
  }

  static getDerivedStateFromError(error: Error) {
    return { error }
  }

  handleCode = (qrCode: QRCode) => {
    if (this.state.entries.some(entry => entry.shcUri === qrCode.data)) {
      throw new Error('Code already scanned');
    }

    const jwsSig = parseCode(qrCode);
    const parts = parseJws(jwsSig);

    const verified = jws.verify(
      jwsSig,
      'ES256',
      this.props.pubkey
    );

    const bundle = parts.payload.vc?.credentialSubject?.fhirBundle;

    if (!bundle) throw new Error("Could not load the bundle.");

    const entry = {
      shcUri: qrCode.data,
      fhirBundle: bundle,
      verified,
    }

    this.setState({
      entries: update(this.state.entries, { $push: [entry] })
    })
  }

  renderEntry(entry: ScanResult) {
    const resources = entry.fhirBundle ?
                      getResources(entry.fhirBundle!) : [];

    if (!resources) return null;

    return (
      <Card shcUri={entry.shcUri!}
            verified={entry.verified!}
            resources={resources} />
    )
  }

  render() {
    const { error } = this.state;

    return (
      <div>
          <div className="no-print">
              <Scanner onScanCode={this.handleCode} />
              {error && <p>{error}</p>}
          </div>
          <div className="App-cards">
              {Array.from(this.state.entries).map(this.renderEntry)}
          </div>
      </div>
    )
  }
}
