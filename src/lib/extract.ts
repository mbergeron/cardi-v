export interface FHIRBundle {
  entry: FHIREntry[]
}

export interface FHIREntry {
  resource: FHIRResource;
}

export interface FHIRResource extends Record<string, any> {
  resourceType: string;
}

export function getResources(fhirBundle: FHIRBundle): FHIRResource[] {
  const resources: FHIRResource[] = [];
  fhirBundle.entry.forEach(entry => resources.push(entry.resource))

  return resources;
}
