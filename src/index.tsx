import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Instructions from './Instructions';
import reportWebVitals from './reportWebVitals';
import { IntlProvider } from 'react-intl'

import './index.css';

// --- I18n
type LANG = 'fr' | 'en';

const language = navigator.language.split(/[-_]/)[0] as LANG;  // language without region code

const messages: Record<LANG, Record<string, string>> = {
  'fr': require('./i18n/fr.json') as Record<string, string>,
  'en': require('./i18n/en.json') as Record<string, string>,
}

const initError = (err: Error) => {
  console.error(err)
  alert('Could not load public key for verification.')
}

fetch('./pubkey.pem')
  .then(res => res.text())
  .then(pubkey => {
      ReactDOM.render(
        <React.StrictMode>
            <IntlProvider messages={messages[language]} locale={language}>
                <App pubkey={pubkey} />
            </IntlProvider>
        </React.StrictMode>,
        document.getElementById('root')
      )
  })
  .catch(initError)

ReactDOM.render(
    <IntlProvider messages={messages[language]} locale={language}>
        <Instructions />
    </IntlProvider>,
  document.getElementById('instructions')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
