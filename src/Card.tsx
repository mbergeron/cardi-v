import React from 'react'
import { FHIRResource } from './lib/extract';
import { QrCode, QrSegment, Ecc } from './lib/qrcodegen';
import { FormattedMessage } from 'react-intl';

import cx from 'classnames'

import './Card.css'

interface PropsType {
  resources: FHIRResource[];
  verified: boolean;
  shcUri: string;
}
// Returns a string of SVG code for an image depicting the given QR Code, with the given number
// of border modules. The string always uses Unix newlines (\n), regardless of the platform.
function qrCodeSvg(qr: QrCode, border: number, lightColor: string, darkColor: string): JSX.Element {
    if (border < 0)
        throw "Border must be non-negative";

    let parts: Array<string> = [];
    for (let y = 0; y < qr.size; y++) {
        for (let x = 0; x < qr.size; x++) {
            if (qr.getModule(x, y))
                parts.push(`M${x + border},${y + border}h1v1h-1z`);
        }
    }

    return (
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox={`0 0 ${qr.size + border * 2} ${qr.size + border * 2}`} stroke="none">
            <rect width="100%" height="100%" fill={lightColor} />
            <path d={parts.join(" ")} fill={darkColor} />
        </svg>
    );
}

export default class Card extends React.PureComponent<PropsType> {
    renderQR() {
        const protocol = QrSegment.makeBytes([0x73, 0x68, 0x63, 0x3a, 0x2f]) // 'shc:/'
        const data = QrSegment.makeNumeric(this.props.shcUri.slice(5));

        const rawCode = QrCode.encodeSegments([protocol, data], Ecc.LOW, 22, 22)
        return qrCodeSvg(rawCode, 0, '#ffffff', '#000000');
    }

    renderResource(resource: FHIRResource, idx: number) {
        switch (resource.resourceType) {
            case "Patient": {
                const { birthDate, name } = resource;
                const { family, given } = name[0];

                return (
                    <div key={idx}>
                        <label><strong>{`Nom: ${family} ${given}`}</strong></label>
                        <label>{birthDate}</label>
                    </div>
                );
            }
            case "Immunization": {
                const { occurrenceDateTime, note } = resource;
                const { text } = note[0];

              return (
                <div key={idx}>
                    <label>{text}</label>
                    <label>{occurrenceDateTime.split("T")[0]}</label>
                </div>
              );
            }
        }
    }

    render() {
        const { resources, shcUri, verified } = this.props;

      if (!shcUri) return null;

      const marker = verified ? "card.verification.verified" : "card.verification.tampered";
      const [ patient, ...doses ] = resources;

        return (
          <div className={cx("card", { 'card--verified': !!verified, 'card--tampered': !verified })}>
                <p>{`${patient.name[0].family} ${patient.name[0].given}`}</p>
                <figure className="qr">
                    {this.renderQR()}
                    <span className="no-print verification">
                        <FormattedMessage id={marker} />
                    </span>
                </figure>
                <p>{patient.birthDate}</p>
            </div>
        )
    }
}
