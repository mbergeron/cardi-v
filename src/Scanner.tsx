import React from 'react'
import jsQR, { QRCode } from 'jsqr'
import { Point } from 'jsqr/dist/locator';

interface PropsType {
    onScanCode: (code: QRCode) => void;
}

export default class Scanner extends React.Component<PropsType> {
    // lazy way to port this over;
    state = {
        video: document.createElement("video"),
        canvas: document.getElementById("canvas") as HTMLCanvasElement,
        loadingMessage: document.getElementById("loadingMessage"),
    }

    componentDidMount() {
        const { video } = this.state;

        // Use facingMode: environment to attemt to get the front camera on phones
        navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function(stream) {
            video.srcObject = stream;
            video.setAttribute("playsinline", "true"); // required to tell iOS safari we don't want fullscreen
            video.play();
        });

        requestAnimationFrame(this.tick.bind(this));
    }

    tick() {
        const {
            canvas,
            loadingMessage,
            video,
        } = this.state;
        const { onScanCode } = this.props;

        const canvasCtx = canvas?.getContext("2d");
        if (!canvasCtx) throw new Error("Could not create 2d canvas context.");

        function drawLine(begin: Point, end: Point, color: string) {
            canvasCtx!.beginPath();
            canvasCtx!.moveTo(begin.x, begin.y);
            canvasCtx!.lineTo(end.x, end.y);
            canvasCtx!.lineWidth = 4;
            canvasCtx!.strokeStyle = color;
            canvasCtx!.stroke();
        }

        loadingMessage!.innerText = "⌛ Loading video..."
        if (video.readyState === video.HAVE_ENOUGH_DATA) {
            loadingMessage!.hidden = true;
            canvas.hidden = false;

            const h = canvas.height = video.videoHeight / 1.5;
            const w = canvas.width = video.videoWidth / 1.5;

            canvasCtx!.drawImage(video, 0, 0, w, h);
            var imageData = canvasCtx!.getImageData(0, 0, w, h);

            var code = jsQR(imageData.data, imageData.width, imageData.height, {
                inversionAttempts: "dontInvert",
            });

            if (code) {
                drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
                drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
                drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
                drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");

              try {
                onScanCode(code)
              } catch(err) {
                console.log(err)
              }
            }
        }

        requestAnimationFrame(this.tick.bind(this));
    }

  render() {
    return null;
  }
}
